package sk.kottman.androlua;

import android.app.Activity;
import android.os.Bundle;

import org.keplerproject.luajava.JCLua;
import org.keplerproject.luajava.LuaException;


public class Main extends Activity {
    String LUA_CODE = "require 'import'\n" +
            "json = require 'json'\n" +
            "\n" +
            "videoOutUrl = \"[-videoOutUrl-]\"\n" +
            "videoFrom = \"[-videoFrom-]\"\n" +
            "videoReferer = \"[-videoReferer-]\"\n" +
            "if (videoFrom == \"bilibili\")\n" +
            "then\n" +
            "    serverUrl = \"http://api.bilibili.com/playurl?callback=jQuery17207329071830067004_1475894345733&aid=\" .. videoOutUrl .. \"&page=1&platform=html5&quality=1&vtype=mp4&type=jsonp&_=1476006495058\"\n" +
            "    data = Http4Lua:getUrl(serverUrl, videoReferer)\n" +
            "    tmp = string.find(data, \"[{]\")\n" +
            "    ret = string.sub(data, tmp, #data - 2)\n" +
            "    local lua_value = json.decode(ret)\n" +
            "    mp4 = lua_value[\"durl\"][1][\"url\"]\n" +
            "    print(mp4)\n" +
            "else\n" +
            "    print(\"default\")\n" +
            "end";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //Tes lua
        //就是两个步骤 1.printURL,得到url请求服务器。2.printMP4,解析JSON给播放器
        //String LUA_CODE = "require 'import'\nprint(new_tostring(\"23ssss\"))\n";
        //String LUA_CODE = "require 'import'\nprint(Http4Lua:getUrl())\n";
        try {
            JCLua.instant().init(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {//AsyncTask 不支持，会报错
            @Override
            public void run() {
                String videoOutUrl = String.valueOf("6554340");
                String videoFrom = String.valueOf("bilibili");
                String videoReferer = String.valueOf("referer");
                String TMP_CODE = LUA_CODE.
                        replace("[-videoOutUrl-]", videoOutUrl).
                        replace("[-videoFrom-]", videoFrom).
                        replace("[-videoReferer-]", videoReferer);
                try {
                    String ret = JCLua.instant().excuse(TMP_CODE);
                    System.out.println("Lua String: " + ret);
                } catch (LuaException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
